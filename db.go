package secrets

import "encoding/json"

type DatabaseConfig struct {
	Address           string
	Database          string
	UserSecretKey     string
	PasswordSecretKey string
	DevEnv            bool
}

type DatabaseCredentials struct {
	Address  string
	Database string
	User     string
	Password string
}

func GetDatabaseCredentials(cfg DatabaseConfig) DatabaseCredentials {
	if cfg.DevEnv {
		return DatabaseCredentials{
			Address:  cfg.Address,
			Database: cfg.Database,
			User:     cfg.UserSecretKey,
			Password: cfg.PasswordSecretKey,
		}
	}

	userSecret, err := GetSecret(cfg.UserSecretKey)
	if err != nil {
		panic(err)
	}

	var secretMap map[string]interface{}
	if err = json.Unmarshal([]byte(userSecret), secretMap); err != nil {
		panic(err)
	}
	username := secretMap["username"].(string)

	passwordSecret, err := GetSecret(cfg.PasswordSecretKey)
	if err != nil {
		panic(err)
	}

	if err = json.Unmarshal([]byte(passwordSecret), secretMap); err != nil {
		panic(err)
	}
	password := secretMap["password"].(string)

	return DatabaseCredentials{
		Address:  cfg.Address,
		Database: cfg.Database,
		User:     username,
		Password: password,
	}
}
